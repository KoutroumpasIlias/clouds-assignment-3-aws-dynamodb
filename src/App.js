import './App.css';
import React from "react";
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {
    Box,
    Button,
    Dialog, DialogActions,
    DialogContentText,
    DialogTitle,
    Divider,
    Grid,
    TextField,
    Typography
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import {uuid} from 'uuidv4';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    paper: {
        width: "100%",
        height: "100vh",
        margin: "auto"
    },
    dialog: {
        padding: "5px",
    },
    divider: {
        color: "red",
    },
    title: {
        color: "black",
        backgroundColor: "white"
    }

});

const headers = {
    "Content-Type": "application/json",
    "accept": "application/json",
};

const baseUrl = "https://60ga0y6qmd.execute-api.us-east-2.amazonaws.com/cars";
const cleanCar = {
    id: null,
    modelname: "",
    productionyear: new Date(),
    color: "",
    price: 0.0
}

function App() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [cars, setCars] = React.useState([]);
    const [newCar, setNewCar] = React.useState({
        modelname: "",
        productionyear: new Date(),
        color: "",
        price: 0.0,
    });
    const [updatedCar, setUpdatedCar] = React.useState({...cleanCar});

    // Fetch all cars
    // eslint-disable-next-line react-hooks/exhaustive-deps
    React.useEffect(async () => {
        const requestOptions = {
            method: "GET",
            headers: headers
        };
        const response = await (await fetch(baseUrl, requestOptions)).json();
        setCars(response.Items);
    }, []);

    const saveCar = async () => {
        const id = uuid();
        const requestOptions = {
            method: "PUT",
            headers: headers,
            body: JSON.stringify({
                ...newCar,
                productionyear: newCar.productionyear.getFullYear(),
                id: id,
            })
        };
        try {
            await (await fetch(baseUrl, requestOptions)).json();
            setCars([...cars, {
                ...newCar,
                productionyear: newCar.productionyear.getFullYear(),
                id: id,
            }]);
        } catch (err) {
            alert("THERE WAS AN ERROR SAVING THE NEW CAR: " + err.message);
        }
    }

    const updateCar =async () => {
        const requestOptions = {
            method: "PUT",
            headers: headers,
            body: JSON.stringify({
                ...updatedCar,
                productionyear: new Date(updatedCar.productionyear).getFullYear()
            })
        };
        try {
            await (await fetch(baseUrl, requestOptions)).json();
            const index = cars.findIndex(c => c.id === updatedCar.id);
            const carsNew = [...cars];
            carsNew[index] = {...updatedCar, productionyear: new Date(updatedCar.productionyear).getFullYear()};
            setCars(carsNew);
        } catch(err) {
            alert("THERE WAS AN ERROR UPDATING THE CAR: " + err.message);
        }
    }

    const getPopup = () => {
        if (!open) return null;
        const close = () => {
            setUpdatedCar({...cleanCar});
            setOpen(false);
        }
        const update = () => {
            updateCar().then(_ => close());
        }
        return (
            <Dialog className={classes.dialog} fullWidth={true} open={open} aria-labelledby="company-dialog-popup">
                <DialogTitle className={classes.title}>
                    {"New Car"}
                </DialogTitle>
                <Divider classes={{
                    root: classes.divider
                }}/>
                <Box mr={2} ml={2} mt={0}>
                    <DialogContentText style={{marginTop: "20px"}}>
                        {"Update the car details below"}
                    </DialogContentText>
                    <Grid container spacing={3} direction='column'>
                        <Grid item>
                            <TextField
                                onChange={(event) =>
                                    setUpdatedCar({...updatedCar, modelname: event.target.value})}
                                autoFocus
                                id="name"
                                label={"Model name"}
                                type="text"
                                fullWidth
                                value={updatedCar.modelname}
                            />
                        </Grid>
                        <Grid item>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker
                                    disableFuture
                                    views={["year"]}
                                    label="Production year"
                                    value={Date.parse(updatedCar.productionyear)}
                                    onChange={(e) => setUpdatedCar({...updatedCar, productionyear: e})}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item>
                            <TextField
                                onChange={(event) =>
                                    setUpdatedCar({...updatedCar, color: event.target.value})}
                                id="color"
                                label={"Color"}
                                type="text"
                                fullWidth
                                value={updatedCar.color}
                            />
                        </Grid>
                        <Grid item>
                            <TextField id={"price"}
                                       value={updatedCar.price}
                                       inputProps={{
                                           step: 0.01,
                                           min: 0
                                       }} type="number" label="Price"
                                       onChange={(e) =>
                                           setUpdatedCar({...updatedCar, price: Number.parseFloat(e.target.value)})}
                            />
                        </Grid>
                    </Grid>
                </Box>
                <DialogActions style={{marginTop: "20px"}}>
                    <Button
                        variant={"filled"}
                        onClick={close}
                        color="secondary"
                    >
                        CANCEL
                    </Button>
                    <Button
                        variant={"filled"}
                        onClick={update}
                        color="primary">
                        UPDATE
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

    const deleteCar = async (id) => {
        const requestOptions = {
            method: "DELETE",
            headers: headers,
        };
        try {
            await (await fetch(`${baseUrl}\\${id}`, requestOptions));
            setCars(cars.filter(c => c.id !== id));
        } catch (err) {
            alert("THERE WAS AN ERROR DELETING THE CAR: " + err.message);
        }
    }

    return (
        <React.Fragment>
            <Grid direction={"column"} className={classes.paper} container justifyContent="center"
                  alignItems={"center"}>
                <Grid item><Typography align={"center"} gutterBottom variant="h2">Cars Table</Typography></Grid>
                <Grid item>
                    <Paper>
                        <TableContainer style={{maxHeight: "70vh"}} component={Paper}>
                            <Table stickyHeader aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>MODEL NAME</TableCell>
                                        <TableCell align="center">COLOR</TableCell>
                                        <TableCell align="center">PRODUCTION YEAR</TableCell>
                                        <TableCell align="center">PRICE</TableCell>
                                        <TableCell align="center">ACTIONS</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {cars.map(c => (
                                        <TableRow key={c.id}>
                                            <TableCell component="th" scope={"row"}>
                                                {c.modelname}
                                            </TableCell>
                                            <TableCell align={"center"}>{c.color}</TableCell>
                                            <TableCell align={"center"}>{c.productionyear}</TableCell>
                                            <TableCell align={"center"}>{c.price}</TableCell>
                                            <TableCell align={"center"}>
                                                <Button onClick={() => {
                                                    setUpdatedCar({...c});
                                                    setOpen(true);
                                                }}>
                                                    Update
                                                </Button>
                                                <Button onClick={() => deleteCar(c.id)}>
                                                    Delete
                                                </Button>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <TableRow>
                            <TableCell component="th" scope={"row"}>
                                <TextField id="new_model_name" label="New model name" value={newCar.modelname}
                                           onChange={(e) =>
                                               setNewCar({...newCar, modelname: e.target.value})}
                                />
                            </TableCell>
                            <TableCell align={"center"}>
                                <TextField id="new_color" label="Color" value={newCar.color}
                                           onChange={(e) =>
                                               setNewCar({...newCar, color: e.target.value})}
                                />
                            </TableCell>
                            <TableCell align={"center"}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <DatePicker
                                        disableFuture
                                        views={["year"]}
                                        label="Production year"
                                        value={newCar.productionyear}
                                        onChange={(e) => setNewCar({...newCar, productionyear: e})}
                                    />
                                </MuiPickersUtilsProvider>
                            </TableCell>
                            <TableCell align={"center"}>
                                <TextField id={"new_price"}
                                           value={newCar.price}
                                           inputProps={{
                                               step: 0.01,
                                               min: 0
                                           }} type="number" label="Price"
                                           onChange={(e) =>
                                               setNewCar({...newCar, price: Number.parseFloat(e.target.value)})}
                                />
                            </TableCell>
                            <TableCell align={"center"}>
                                <Button onClick={saveCar}>Save</Button>
                            </TableCell>
                        </TableRow>
                    </Paper>
                </Grid>
            </Grid>
            {getPopup()}
        </React.Fragment>
    );
}

export default App;
